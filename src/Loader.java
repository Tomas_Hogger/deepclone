import java.io.IOException;
import java.util.HashMap;

public class Loader {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        B b = new B();
        b.b = 1;
        b.c = 2;
        b.i = 3;
        b.q = "qw";
        b.a = new A();
        b.a.j = 1;
        b.a.a = new A();
        b.hashMap = new HashMap<>();
        b.hashMap.put(1,1);

        B b1 = (B) DeepCopy.deepCopy(b);
        b1.b = 2;
        b1.a = new A();
        b1.a.j = 5;

    }



}
